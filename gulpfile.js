var gulp = require('gulp');

// include plug-ins
var concat = require('gulp-concat');
var glob = require("glob");
var mainBowerFiles = require('main-bower-files');
var gulpFilter = require('gulp-filter');
var minify = require('gulp-minify');
var fs = require("fs");
gulp.task("buildJavascript", function () {
	console.log("building javascript")
	gulp.src(["webapp/scripts/**/*.js"])
		.pipe(concat('build.js'))
		.pipe(gulp.dest('webapp/build'));
})

gulp.task("buildCss", function () {
	gulp.src(["webapp/css/**/*.css"])
		.pipe(concat('build.css'))
		.pipe(gulp.dest('webapp/build'));
})

gulp.task("buildVendorCss", function () {
	console.log((mainBowerFiles({
		paths: {
			bowerDirectory: "bower_components",
			bowerJson: "bower.json"
		}
	})));
	return gulp.src(mainBowerFiles({
		paths: {
			bowerDirectory: "bower_components",
			bowerJson: "bower.json"
		}
	}))
		.pipe(gulpFilter("**/*.css"))
		.pipe(concat('vendor.css'))
		.pipe(gulp.dest('webapp/build'));
})

gulp.task("buildVendorJavascript", function () {
	return gulp.src(mainBowerFiles({
		paths: {
			bowerDirectory: "bower_components",
			bowerJson: "bower.json"
		}
	}))
		.pipe(gulpFilter("**/*.js"))
		.pipe(concat("vendor.js"))
		.pipe(minify({
			ext: {
				min: ".js"
			},
			noSource: true
		}))
		.pipe(gulp.dest("webapp/build"));
})


gulp.task("setupEnv", ()=>{
	if(!fs.existsSync(".env")){
		fs.copyFileSync(".env.template",".env")
	}
})

gulp.task('build', ["setupEnv", "buildVendorJavascript", "buildJavascript", "buildVendorCss", "buildCss"])



gulp.task('watch', function () {
	return gulp.watch(["webapp/scripts/**/*.js", "webapp/scripts/*.js", "webapp/css/*.css"], ["build"])
})

gulp.task('default', ['build', 'watch']);