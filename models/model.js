var mongoose = require('mongoose');
var q = require("q");
mongoose.Promise = q.Promise;
var Schema = mongoose.Schema;
var UserSchema = new Schema({
	username: String,
	email: String,
	passwordHash: String,
	isAdmin: Boolean,
	isLoginAllowed: Boolean
});

var CommentSchema = new Schema({
	content: String,
	timestamp: { type: Date, default: Date.now },
	submitter: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
});

var QuoteSchema = new Schema({
	title: String,
	content: String,
	timestamp: { type: Date, default: Date.now },
	cited: String,
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Comment" }],
	submitter: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
});

var ImageSchema = new Schema({
	filename: String,
	title: String,
	description: String,
	timestamp: { type: Date, default: Date.now },
	comments: [{ type: mongoose.Schema.Types.ObjectId, ref: "Comment" }],
	submitter: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
})

mongoose.connection.openUri(process.env.MONGODB_URI);



var User = mongoose.model("User", UserSchema);
var Quote = mongoose.model("Quote", QuoteSchema);
var Comment = mongoose.model("Comment", CommentSchema);
var Image = mongoose.model("Image", ImageSchema);

module.exports.User = User;
module.exports.Quote = Quote;
module.exports.Comment = Comment;
module.exports.Image = Image;