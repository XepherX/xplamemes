-- phpMyAdmin SQL Dump 
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Server-Version: 5.5.52-MariaDB
-- PHP-Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ISTD`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Entry`
--

CREATE TABLE `Entry` (
  `EntryID` int(11) NOT NULL,
  `EntryType` int(11) NOT NULL,
  `EntryCon` text NOT NULL,
  `EntryDeclar` text,
  `UserID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `UpdateDateTime` datetime DEFAULT NULL,
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DataAreaID` int(11) NOT NULL,
  `RecId` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Entry`
--

INSERT INTO `Entry` (`EntryID`, `EntryType`, `EntryCon`, `EntryDeclar`, `UserID`, `Status`, `UpdateDateTime`, `CreationTimestamp`, `DataAreaID`, `RecId`) VALUES
(502, 1, 'Wiä machts?', NULL, 6, 0, NULL, '2012-01-04 21:33:00', 2, '3035d2f098240313893fc98b45167fa4'),
(504, 1, 'Tuuuuusig', NULL, 8, 0, NULL, '2012-01-05 12:11:00', 2, 'dd65579ac3cae7a39ce50dd599486b5f'),
(505, 1, 'Hebab Télé', NULL, 6, 0, NULL, '2012-01-05 12:21:00', 2, 'b380406317aeff0d8cfd40919dcc521e'),
(506, 1, 'nicht (t)rollen', NULL, 9, 0, NULL, '2012-01-05 13:50:00', 2, 'cd0ff8a613c386f7709e2caa8afc014c'),
(507, 1, 'sud', NULL, 9, 0, NULL, '2012-01-05 13:51:00', 2, '07038fcf0a83ea130f5d6eb527c543f3'),
(508, 1, 'sone sog', NULL, 9, 0, NULL, '2012-01-05 13:51:00', 2, 'b86b1754c8bc140ca7f0d32051e1a1bc'),
(509, 1, 'standard', NULL, 9, 0, NULL, '2012-01-05 13:53:00', 2, 'a096b47eddf65bf9b4cc88d8373eecc5'),
(510, 1, 'pflicht', NULL, 9, 0, NULL, '2012-01-05 13:54:00', 2, '07bc6be2c8cff083fe87ef619533a693'),
(511, 1, 'Chilly McChills', NULL, 9, 0, NULL, '2012-01-05 13:54:00', 2, '78fc86dbcead7b0791ae8de21b74a404'),
(512, 1, 'Ufpassä ghäu!!!', NULL, 6, 0, NULL, '2012-01-05 14:46:00', 2, 'f5fbe1efc4bc883b46706518363c083d'),
(513, 1, 'Findi guet', NULL, 8, 0, NULL, '2012-01-05 14:47:00', 2, '93034250a7c35e8617745946d1f7d814'),
(514, 1, 'xCheffo', NULL, 6, 0, NULL, '2012-01-05 14:50:00', 2, '727ec717ebecd8e99b41105e3ade4419'),
(515, 1, 'Krasses äm-3', NULL, 6, 0, NULL, '2012-01-05 14:50:00', 2, 'abe3aac53e81f15a2f2b012aa9d9b61c'),
(516, 1, 'DB-Ürsu', NULL, 6, 0, NULL, '2012-01-05 14:57:00', 2, '3da912c3332cb2bc7192ccf3635ec7a8'),
(517, 1, 'RideShare-Fränä', NULL, 6, 0, NULL, '2012-01-05 14:58:00', 2, '58bf868d5b856fd670eb5446789e60fc'),
(518, 1, 'IT-Andi', NULL, 8, 0, NULL, '2012-01-05 15:03:00', 2, '1f590dfad8aadedfef225afd1f31834a'),
(519, 1, 'BSH-Bäsu', NULL, 8, 0, NULL, '2012-01-05 15:04:00', 2, 'b4c2a5c560591f435f61ad3d0aee4286'),
(520, 1, 'Intertour Schtrategie', NULL, 8, 0, NULL, '2012-01-05 15:05:00', 2, '27d070eb819c19a36874b0e6ba5d6e6b'),
(521, 1, 'Inelah, ussselah', NULL, 8, 0, NULL, '2012-01-05 15:05:00', 2, '0fe6d9e3da2bc14db86188878a9f752d'),
(522, 1, 'Photosynthese', 'Ist Andrea, der im &quot;Töggelä&quot; den Schuss vom Verteidiger gleich/sofort wieder aufs Tor zurückschiesst und im Idealfall ein Kontertor erzielt.', 8, 0, '2013-05-13 10:09:46', '2012-01-05 15:13:00', 2, '4b878c11e98decab1df3001e9beb78d8'),
(523, 1, 'Edeffo', NULL, 6, 0, NULL, '2012-01-05 15:17:00', 2, '0487434fc52705d0402360e0a8ac276f'),
(524, 1, 'Jäcky', NULL, 6, 0, NULL, '2012-01-05 15:17:00', 2, '3c3a7877abc75aeff9c3ca716d70c0d0'),
(525, 1, 'Es Share im Share? Isch ja wie Inception!', NULL, 6, 0, NULL, '2012-01-05 15:19:00', 2, 'a153d3b62f4eb5d60c39e6f58ad23609'),
(526, 1, 'Wart schnäu, lahmi guet überlege.... NEI', NULL, 8, 0, NULL, '2012-01-05 15:20:00', 2, 'c146e964e77bf2ad6e851eb8a25d0d95'),
(527, 1, 'Der AX-Batch isch tschuld', NULL, 8, 0, NULL, '2012-01-05 15:21:00', 2, '2e29f3ac0b3616af250ebb71b76b39a6'),
(528, 1, 'LP>AX', NULL, 6, 0, NULL, '2012-01-05 15:22:00', 2, '18075203c314565b1debc15437c7e5c0'),
(529, 1, 'Frou Coop', NULL, 6, 0, NULL, '2012-01-05 15:23:00', 2, '2aa7be0f78488f93ad85909e677cabb1'),
(530, 1, 'LP 2.1 ahh nei L2P1', NULL, 8, 0, NULL, '2012-01-05 15:23:00', 2, '05bb68eacf25ccfc887646c6fafe8153'),
(531, 1, 'Am Elmiger sini verpixlätä Screenshots!', NULL, 6, 0, NULL, '2012-01-05 15:30:00', 2, 'b1a3deccb6dc3357a1f32d88d1ec1384'),
(532, 1, 'UNKNOWN EXEPTION', NULL, 8, 0, NULL, '2012-01-05 15:32:00', 2, 'b298d60288c058b9661c4891b38554ce'),
(533, 1, 'Hey Andi, Mary wot imfau no eh VM', NULL, 6, 0, NULL, '2012-01-05 15:34:00', 2, '58ce1d46717fd0b4662e7ea8aa860463'),
(534, 1, 'English Privatunterricht', NULL, 6, 0, NULL, '2012-01-05 15:35:00', 2, '0c9cab80d0ee3adf73a355c71fa14616'),
(535, 1, 'Elia, organisier mau eh LAN!', NULL, 6, 0, NULL, '2012-01-05 15:37:00', 2, '4edcc661812a11e0f7bfe1895782d2e9'),
(536, 1, 'Andi, sibbä! sibbä! sibbä!!!', NULL, 6, 0, NULL, '2012-01-05 15:38:00', 2, '1118d4a869a424f6abaa379bc3f19cb7'),
(537, 1, 'commitmem', NULL, 9, 0, NULL, '2012-01-05 18:17:00', 2, '8f9f2b29728e950168dcb04cef561608'),
(538, 1, 'scröm', NULL, 9, 0, NULL, '2012-01-05 18:18:00', 2, '972dc5d7c149460751fd439527507f96'),
(539, 1, 'räffus poker rebuys', NULL, 9, 0, NULL, '2012-01-05 18:20:00', 2, 'fc4d0c36a77ecb956a66af034eb71d49'),
(540, 1, 'BO Drache', NULL, 9, 0, NULL, '2012-01-05 18:20:00', 2, '8e67162b0f344a43e98f531235bfa422'),
(541, 1, 'Abbruchfest', NULL, 9, 0, NULL, '2012-01-05 18:21:00', 2, '44fff18b8606ee09cec16544f6d18cc0'),
(542, 1, 'Pinöps', NULL, 8, 0, NULL, '2012-01-05 20:09:00', 2, 'f27a1056912a3025062b50f1d9713670'),
(543, 1, 'Reeeeenuaaageee', NULL, 8, 0, NULL, '2012-01-05 20:10:00', 2, '6db58dbfdeac916f409bd4800fec123a'),
(544, 1, 'Full-in', NULL, 8, 0, NULL, '2012-01-05 20:10:00', 2, 'f61bb4d5adede8a1449215af1be50a9e'),
(545, 1, 'Basil und die Banane am Abbruchfest', NULL, 6, 0, NULL, '2012-01-06 06:54:00', 2, '2038e58aa960dcdd979f34135a9795f9'),
(546, 1, 'Suppenmittag', NULL, 6, 0, NULL, '2012-01-06 10:49:00', 2, '0c2b5b34d6513c59b2614f09413e3846'),
(547, 1, '2 Girls 1 Coup-Link', NULL, 6, 0, NULL, '2012-01-06 13:17:00', 2, '5d9f5b710d1a4ebb177bcacc6c77ed40'),
(548, 1, 'Ehhh, Ronny...', NULL, 8, 0, NULL, '2012-01-07 07:55:00', 2, '33601e4e64c84eccc130147e045907ac'),
(549, 1, 'Cordonbleu usem Tramway', NULL, 8, 0, NULL, '2012-01-07 07:58:00', 2, '1f166cea42d63b338415c58c12ae6f8b'),
(550, 1, 'Alles löschen?', NULL, 9, 0, NULL, '2012-01-07 18:09:00', 2, '6fc21c14c37f420a1874e61aa94aaa48'),
(551, 1, 'so fägts!', NULL, 9, 0, NULL, '2012-01-07 18:09:00', 2, '0d9836c032bc01ba8a317237f3f14f6f'),
(552, 1, 'Interdispatch', NULL, 8, 0, NULL, '2012-01-08 11:18:00', 2, 'd291c9de9de4bba3870b1de396e8c9dc'),
(553, 1, 'Z\'PTV geit nid...', NULL, 8, 0, NULL, '2012-01-08 11:19:00', 2, '11ae58a3940dd998c804b7e4ce5988c9'),
(554, 1, 'Bisch du e ritter?', NULL, 8, 0, NULL, '2012-01-09 07:17:00', 2, 'dabf64a663fd6065272e44c8a9248cd6'),
(555, 1, 'Porsche Cayenne - Sportwagenfan', NULL, 6, 0, NULL, '2012-01-09 08:33:00', 2, '32c6b4d05f954c1e268cc183e1d99479'),
(557, 1, 'Out of the box', NULL, 14, 0, NULL, '2012-01-09 08:55:00', 2, '4e73a22eee14d6515514225a80eca4d1'),
(558, 1, '*** SCHISS AG', NULL, 6, 1, NULL, '2012-01-09 09:51:00', 2, '41661b3dfbb34315ecb5b6e9dd84674b'),
(559, 1, 'Beschti Kollegin :-?', NULL, 6, 0, NULL, '2012-01-09 11:00:00', 2, 'ace6b0f44839ad8e77217b179bb72f63'),
(560, 1, 'Bugfree-version', NULL, 8, 0, NULL, '2012-01-09 11:02:00', 2, '0cacbdcf9638127554c53eb1901e4cde'),
(561, 1, 'Anna uf eh CC', NULL, 6, 0, NULL, '2012-01-09 11:04:00', 2, '5fd085472fbc55fb7dfa1eb14fe3cf57'),
(562, 1, 'Hallo WERNER!!!', NULL, 8, 0, NULL, '2012-01-09 11:08:00', 2, 'b63fc6db6f947788f7aa19b2874e8812'),
(563, 1, 'Sii, Herr Baly', NULL, 8, 0, NULL, '2012-01-09 11:15:00', 2, 'de91be6d8eadcd108e1adcb5fa3e7e8c'),
(564, 1, 'Sie, ich glaub dä brännt...', NULL, 8, 0, NULL, '2012-01-09 11:16:00', 2, 'dd1dbaa6db65fd16b4e23c05178e5642'),
(565, 1, 'Bitzius cs map', NULL, 8, 0, NULL, '2012-01-09 18:04:00', 2, 'c136d39b8e867acba6f7fc33d3a38476'),
(566, 1, 'Solution provider', NULL, 14, 0, NULL, '2012-01-09 19:11:00', 2, '148c8c88a32b3639f26865c526651c38'),
(567, 1, 'Suxor', NULL, 6, 0, NULL, '2012-01-10 08:19:00', 2, '4e378a02a739951403eef3437ab9e549'),
(568, 1, 'Gucci Bandana', NULL, 15, 0, NULL, '2012-01-10 10:16:00', 2, 'c3cb950f64de2539ff02ec6105443987'),
(569, 1, 'Werner@Skype: Hallo', NULL, 6, 0, NULL, '2012-01-10 11:00:00', 2, '7abb5c0d2052f27af86fa49d7d378ce0'),
(570, 1, 'PTV 5-minuten', NULL, 8, 0, NULL, '2012-01-10 11:13:00', 2, '10d3c5a8d7e01d65910a62c9ce7cf756'),
(571, 1, 'Landi, Wandi, Gandi', NULL, 6, 0, NULL, '2012-01-10 11:22:00', 2, 'f4bc5b53964300d25a0937528431009a'),
(572, 1, 'Ä früdu ga zieh', 'Ufs WC gha', 8, 0, '2013-06-05 14:05:55', '2012-01-10 11:45:00', 2, '767a6556ba9838c0ec2aa83996930a85'),
(573, 1, 'Fire and forgett', NULL, 8, 0, NULL, '2012-01-10 11:46:00', 2, 'ef9a1f1d1ff049ed4fc20f620a350a62'),
(574, 1, 'Multiball', NULL, 6, 1, NULL, '2012-01-10 15:48:00', 2, '0f3e92ad1eb7a0cd2bd882e79be83dfb'),
(575, 1, 'Noooo!! E M aus-ge-schiedeeen!', NULL, 14, 0, NULL, '2012-01-10 16:46:00', 2, 'fdfeb68c43fdffd735f744f5fb507fd3'),
(580, 1, 'stächsi, lachsi, stächzäni, stichzäni, flachzäni und schwänzgi.', NULL, 6, 0, NULL, '2012-01-10 22:07:00', 2, '031b6bdcd92e3b7f96410b8250282982'),
(581, 1, 'Optionen Yves', NULL, 14, 0, NULL, '2012-01-11 09:20:00', 2, 'd31fdd29dd439c12e9ca1c6fe74e5d74'),
(582, 1, '...und schieß mich tot!', NULL, 14, 0, NULL, '2012-01-11 17:31:00', 2, 'f0dbe47374951a57967de7e7c24196c7'),
(583, 1, 'Na ah!', NULL, 14, 0, NULL, '2012-01-11 17:38:00', 2, '7b9119b8c6f421745f13cfc78a5b7e59'),
(584, 1, 'Atomics', NULL, 9, 0, NULL, '2012-01-11 22:51:00', 2, '79660988512f4d109cff3f470bf3e54b'),
(585, 1, 'Soo Schwul!', NULL, 14, 0, NULL, '2012-01-12 05:20:00', 2, '23881e25911368300a6c3fe088f442e9'),
(586, 1, 'Boom Selecta!', NULL, 14, 0, NULL, '2012-01-13 04:56:00', 2, 'b35945edcd14393e81806ad442c76329'),
(587, 1, 'Beat Zéllé', NULL, 6, 0, NULL, '2012-01-13 11:32:00', 2, '13315d468d70afa17b7fb1c06bcd17c9'),
(588, 1, 'Cäscher Bässu', NULL, 6, 0, NULL, '2012-01-13 17:45:00', 2, '4540206462d6b937baa46e1c2b80a3de'),
(589, 1, 'Multiball', NULL, 8, 0, NULL, '2012-01-14 18:29:00', 2, '65a553be805731d2cebbb1f35889e4f4'),
(590, 1, 'Tschaira', NULL, 14, 0, NULL, '2012-01-15 11:59:00', 2, 'c469605cdac3f3f70de2139dd5d7effa'),
(591, 1, 'Der Leduc wo vom Raff id Wang klepft wird', NULL, 8, 0, NULL, '2012-01-16 14:55:00', 2, '08afd84a12843749299e40a5e73f02b9'),
(592, 1, 'I\'ll be Beck!', NULL, 14, 0, NULL, '2012-01-16 18:50:00', 2, 'ccb3a757f89b2c14ac136e8ea95f7096'),
(593, 1, 'hulli@gmx.ch', NULL, 6, 0, NULL, '2012-01-18 16:43:00', 2, '89d4c32f6e129d2def088a24f7d7232c'),
(594, 1, 'Wieso ist morn pflicht und heute nicht?', '', 6, 0, '2013-05-29 16:35:14', '2012-01-18 18:21:00', 2, '8ac781d1a17f808908af729ab94f25e5'),
(596, 1, 'Sufil', NULL, 8, 0, NULL, '2012-01-19 21:13:00', 2, '493590c52ce64b91964ca2085db92ae4'),
(597, 1, 'Sig-Sag-Sug - Ih fahrä!!!', NULL, 6, 0, NULL, '2012-01-19 22:53:00', 2, '6acff74a8ac90d9e7e3b0360115ddaae'),
(599, 1, 'Ja, ih lueges ah!', NULL, 6, 0, NULL, '2012-01-20 14:35:00', 2, '568b15b68279d20c9481dfb09ec56d12'),
(600, 1, 'Es sind keine Standards! ', NULL, 14, 0, NULL, '2012-01-20 17:55:00', 2, 'b6c70801bceeb95b84860254047f7e81'),
(601, 1, 'Hey! Aber... Bitte!!!', NULL, 14, 0, NULL, '2012-01-21 14:45:00', 2, 'e488da1b82e83633a73d1ff2ecdfa0e7'),
(603, 1, 'Kantonspolizei, Moser!', NULL, 14, 0, NULL, '2012-01-22 10:35:00', 2, '257cfe4b6fe42a06f850155bd1777f0e'),
(604, 1, 'Selecta top flop', NULL, 14, 0, NULL, '2012-01-22 13:52:00', 2, '960a9a8c6ea2e8f701adedd3769eb06b'),
(605, 1, 'Chillaroora!\r\nChills man!', NULL, 14, 0, NULL, '2012-01-22 19:19:00', 2, 'cd1ce42a83105c9f699559a2a199ce27'),
(606, 1, 'PTV SWISS...       ...Brunner', NULL, 6, 0, NULL, '2012-01-23 11:12:00', 2, 'e139e04b83c986e34b7fe992709dd65e'),
(607, 1, '20Min: "Wir pflichten aus tiefen Herzen"', NULL, 6, 0, NULL, '2012-01-23 11:19:00', 2, '32d403c12e0d0cc39619eb06e2836fee'),
(608, 1, '1337Time', NULL, 6, 0, NULL, '2012-01-23 11:37:00', 2, 'b57417c08d5c23f94fe9d75fa7c8878b'),
(609, 1, 'Carmen', NULL, 6, 0, NULL, '2012-01-23 18:07:00', 2, '39b1c45082b994150fae98d6fb21c9f8'),
(610, 1, 'Am Flo sini Euros!', NULL, 6, 1, NULL, '2012-01-24 10:14:00', 2, '75c2ec0a41b206b4dcb1b4cbde10d37c'),
(611, 1, 'Bitte eure Schulde an der Eule zahlen!!', NULL, 14, 0, NULL, '2012-01-24 17:52:00', 2, '6df02338ffd725a4bdd62ad855640c6d'),
(612, 1, 'Torben', NULL, 6, 0, NULL, '2012-01-24 20:00:00', 2, '5061206ee866460d99316ed822991591'),
(613, 1, 'Löiiiä', NULL, 6, 0, NULL, '2012-01-24 20:11:00', 2, '90eb058d0768a15a8ecb50721faca8bd'),
(614, 1, 'Männlich', NULL, 9, 0, NULL, '2012-01-26 21:28:00', 2, '3b5aa282e1fc9b21aebfdd87c1f39e5d'),
(615, 1, 'Flower power Tee für Meitschi', NULL, 9, 0, NULL, '2012-01-26 21:29:00', 2, '8a16f6a10a8b0a86006cccf8ed6dd089'),
(638, 1, 'Barstreet = sex and the city, gaskessel = alpha centauri im quadrat', NULL, 14, 0, NULL, '2012-01-30 05:31:00', 2, '6d50832ab3f8a2b80232025f7678bdb9'),
(639, 1, 'Keine Ahnung Altaa, sag einfach irgendeine Saahl!', NULL, 6, 0, NULL, '2012-01-30 07:46:00', 2, '805de4cd906232c40615a30a549370cd'),
(640, 1, 'Köstliches Getränk', NULL, 6, 0, NULL, '2012-01-30 10:24:00', 2, 'd7b9b7a9d229e3c06cf48c3b8e0814e5'),
(642, 1, 'Sonnenschiessdreck!', NULL, 14, 0, NULL, '2012-01-31 10:09:00', 2, 'ade295580a7e00d31ddad40639b20457'),
(643, 1, 'S-haare für share', NULL, 14, 0, NULL, '2012-02-01 19:17:00', 2, 'e9d5af67079cd72c07bd3c9fd297ac78'),
(644, 1, 'Don\'t give up... Don\'t give up!', NULL, 6, 0, NULL, '2012-02-02 14:33:00', 2, '8af2c3bbd70f0de650289b4b0660b39a'),
(656, 1, 'The Teupelisator', NULL, 9, 0, NULL, '2012-02-03 14:48:00', 2, '8c25f09c5fb2f88a5de8e70a6f56a032'),
(659, 1, 'Tö1pelifloh', NULL, 8, 0, NULL, '2012-02-04 08:20:00', 2, 'c8318661a349f5ab712976f934e0e70b'),
(660, 1, 'Dr Excel-Typ', NULL, 6, 0, NULL, '2012-02-06 12:03:00', 2, 'fc101fe8ecd45a430a317d7704503bbb'),
(661, 1, 'Fingerabdruckscanner', NULL, 6, 0, NULL, '2012-02-06 12:08:00', 2, 'c98d720988e03d32a06bd80bb12f24d6'),
(662, 1, 'Glas muss nach dem streichen mit Gyso Flam Sil 754 versiegelt werden', NULL, 6, 0, NULL, '2012-02-07 09:25:00', 2, '93abc11257238610799291096c9475f9'),
(663, 1, 'Die Heizungen von PTV...', NULL, 14, 0, NULL, '2012-02-09 08:51:00', 2, '333778ac4db047d26db35073b50e7b4c'),
(664, 1, 'Derbi', NULL, 14, 0, NULL, '2012-02-10 19:11:00', 2, '5bca8c4c614dfcf4535a52c047276fc4'),
(665, 1, 'Selecta top flop', NULL, 14, 0, NULL, '2012-02-10 19:16:00', 2, 'fffbba0a8a322b7606dcb1767437f111'),
(666, 1, 'Dr Cässu Drache', NULL, 6, 0, NULL, '2012-02-14 10:31:00', 2, 'e219bcf49f5474c66de34b54f50ced8a'),
(667, 1, 'Ötti Mc Ött', NULL, 6, 0, NULL, '2012-02-14 12:51:00', 2, '0af2248aa913b4a69a4d834341e31e8a'),
(668, 1, 'Winääämore!', NULL, 14, 0, NULL, '2012-02-25 19:09:00', 2, 'd09b8fccfe00af9371007e20a47c12f3'),
(669, 1, 'Herrgott wo esch jetzt dä Uftrag!!!!', NULL, 14, 0, NULL, '2012-03-06 15:33:00', 2, '2c4cbfd830b126f94d5bc31a71a09830'),
(670, 1, 'Muess dä so töne?\r\nJa das isch e V8 dä muess so...', NULL, 8, 0, NULL, '2012-04-06 16:59:00', 2, '020a2c3de42175842b68f89b6d45c3b2'),
(671, 1, 'Ir grossä Hauä, bir Bar, der wo dr Töff steit!', 'dNina wo ar Süri het gschaffet und erklärt het wo iri Bar bar', 6, 0, '2013-05-06 12:07:36', '2012-05-10 16:36:00', 2, '8e89f5c122b00b0443e1484ac4321ec2'),
(672, 1, 'Crystal Jake', NULL, 6, 0, NULL, '2012-05-10 17:28:00', 2, '6eead256a13478083e2d7a399ee93dcd'),
(673, 1, 'Note pad', NULL, 6, 0, NULL, '2012-05-10 17:59:00', 2, 'fae7cced0683a508664a3f7bfb6edf3b'),
(674, 1, 'Nei du?', NULL, 6, 0, NULL, '2012-05-10 18:53:00', 2, '2502c74a185e46bfb53399a4db7c27e2'),
(675, 1, 'Grüäzi, da isch Hotline vo dä PestaGrieser AG vom Support...', NULL, 6, 0, NULL, '2012-07-30 13:41:00', 2, 'b4536af40661cb927edcdb9ccb921d6a'),
(676, 1, 'xPlanis?', NULL, 6, 0, NULL, '2012-08-31 05:27:00', 2, '22396e1cb1deab10bc3de848fe73fbd9'),
(677, 1, 'Jah, gönder...', NULL, 6, 0, NULL, '2012-09-10 09:27:00', 2, 'e85de0393afc7e185819b380b2359447'),
(678, 1, 'Überbache mit Chääs isch aues guet', NULL, 6, 0, NULL, '2012-09-14 11:24:00', 2, '4e41f66b70cbbea93fb259bd51236163'),
(679, 1, 'Tschau merci tschau, merci tschau', NULL, 6, 0, NULL, '2012-10-08 11:54:00', 2, '4b3b67cebe876558d3232d41435d6fc6'),
(680, 1, 'Mit em Outo über Militär Stifu fahre!', NULL, 6, 0, NULL, '2012-10-12 09:00:00', 2, 'fd5b525624a46050e731864cc4c0f7cc'),
(683, 1, 'Mach mau di Chopf zuä!', NULL, 6, 0, NULL, '2013-04-18 10:28:00', 2, '4c6497fbb69cef123dd416aa166b5612'),
(684, 1, 'Webmail installiärä', NULL, 6, 0, NULL, '2013-04-18 10:28:00', 2, '1231ab3f30092100c7d391da88c2919b'),
(686, 1, 'einisch d frässe ha, zum mitnäh', NULL, 49, 0, NULL, '2013-04-18 11:06:00', 2, '3cc480c2634c2a26073eaaf72fdfcf40'),
(687, 1, 'Gülle Gefäs', NULL, 6, 0, NULL, '2013-04-19 08:13:00', 2, 'df17ea5007673f5d2466a4121f20ae0d'),
(688, 1, 'Der Schumi', NULL, 8, 0, NULL, '2013-04-19 08:34:00', 2, 'bbec4db99e031bd0501ec8b1f4ba2698'),
(689, 2, '8db86ecf9f78b25deed0db1ba8d8aa34', NULL, 6, 0, NULL, '2013-05-05 20:52:45', 2, 'be78fcbf020e1062f376337494041d28'),
(697, 1, 'müemer teeschtää !!', 'Teste mit andi', 39, 0, NULL, '2013-05-06 07:28:32', 2, 'f19f63e1f57210212661d6473c1bb98f'),
(698, 1, 'Eeeh Flo, tue mau SmarTour fachläch ihfüärä! ', '-Ruedi', 6, 0, NULL, '2013-05-06 07:38:46', 2, '132016392ac6bdcf77d000b98e40b391'),
(699, 2, '8d09c1f0d1a390c46464b781e770ec7f', NULL, 6, 0, NULL, '2013-05-06 07:46:42', 2, '06fae65bd50044a24e0c9cb121cc3ed3'),
(700, 2, 'b5e88ebb18bd2fc4dfd58d7c3965571d', NULL, 6, 0, NULL, '2013-05-06 07:57:54', 2, '2c03a1ed259061b99ea641a1763b7b46'),
(701, 2, '7f4a1555b27964556f79d074d20e5de6', NULL, 6, 0, NULL, '2013-05-06 07:57:59', 2, 'c9f1ba1133002cc03cc07bf93a28ffd1'),
(702, 2, '553cd80f4315d4266a0a6ee72d9f49a3', 'Chli failet u so', 49, 1, '2013-05-06 10:41:39', '2013-05-06 08:37:07', 2, '76959b8a5d8e46a9a81680a9b64c9826'),
(711, 2, 'c1363c9afa61cf0ec0620afacf064df4', NULL, 6, 0, NULL, '2013-05-06 10:26:10', 2, 'fdd9e462ba79afb2dc0cbc12277a9355'),
(712, 1, 'Easteregg Félé', '', 6, 0, NULL, '2013-05-06 12:36:54', 2, 'b2cec80707f9438174d99370998efd28'),
(713, 1, 'Webäpp', '', 6, 0, NULL, '2013-05-06 12:37:27', 2, '361d768fa310aee860f90d1bff9f52db'),
(714, 1, 'Dä huere rucksack', 'Am Felle si Rucksack wo geng bir Türe steit', 49, 0, '2013-05-06 16:17:20', '2013-05-06 12:44:08', 2, '3d0b21e4fb4a6da34da45b8604091c8e'),
(715, 2, 'e80edfd411f5f165f21e53a73097d2f5', NULL, 6, 0, NULL, '2013-05-06 18:43:00', 2, 'b0d60cc1a7bb4559e97b462ce5271ce3'),
(716, 2, 'be99875365f53aacde0f59fdeeee40b1', NULL, 6, 0, NULL, '2013-05-07 08:54:51', 2, '8d0e14c0d3a7812a7bcd126735784fca'),
(717, 2, 'ee20732fe67fd694265d6dc530f43ab6', NULL, 6, 0, NULL, '2013-05-07 09:00:31', 2, 'f3d12b0d1c9b81a4acd44606eb5eb614'),
(724, 2, '5c98f66462e08a013a3cd4a428fbbec8', NULL, 6, 0, NULL, '2013-05-08 07:33:57', 2, 'a2e8a89bde98eabeee9f915e75eb9c16'),
(722, 2, '21ba1b3422cfd037b477126efde24289', NULL, 49, 1, NULL, '2013-05-07 10:56:47', 2, '32e04573c001782153251d56f5fa6e95'),
(741, 2, 'ff1cdec8eab58fcce1b0fe48cd9c61bf', NULL, 6, 0, NULL, '2013-05-13 14:42:37', 2, '8cf9c0796ba04f99994b2a9b03da9d9b'),
(742, 1, 'Gerat wird entkälkt', '- Andi', 49, 0, '2013-05-13 16:55:08', '2013-05-13 14:51:35', 2, 'b9033d0b97def639b3d426910396dae8'),
(743, 1, 'Overtime Bässu', 'Bässu wos im Ice Rage immer verkackt!', 6, 0, '2013-06-04 18:21:55', '2013-05-17 13:56:31', 2, '388dc82a2a642c59816e52bbcd24801b'),
(756, 2, 'bda9c5488c276a52803ef1eeac3a0c24', NULL, 49, 0, NULL, '2013-05-29 11:35:19', 2, '6b00a9113553884f4d1370d728f6ea6a'),
(757, 1, 'Grüäsäch Herr Moser', 'Dr Andi im Peking Restaurant', 6, 0, '2013-05-29 16:21:22', '2013-05-29 13:42:04', 2, '2fc9038226e0ec5f2c50eabb47ce397f'),
(759, 1, 'Werner@Skype: Meri', 'Vertipper ;)', 49, 0, NULL, '2013-06-04 13:42:12', 2, 'c450bf477989ac946f63543c5c71b295'),
(760, 2, 'd1f79d2fc5179dcf9216a50da3f3564a', NULL, 49, 0, NULL, '2013-06-25 12:52:18', 2, '01458d2e51509cbc913850aaa39ba213'),
(761, 1, 'Werner: Wo göht er ga zmittag ässe?', 'Wenn das Wasser mal wieder alle ist...', 50, 0, NULL, '2013-06-28 08:54:32', 2, 'ce8c51c9fb8c00febe2060fdfca52d7d'),
(762, 2, 'cc75154ddfbe297e980c6c8bc3cf6062', NULL, 9, 0, NULL, '2013-06-28 14:32:08', 2, '5c5fbc808ed835ed09969ebba5acaaa2'),
(763, 2, 'dbe1c2045e8eb6904f28bcbc1630f7ae', NULL, 9, 0, NULL, '2013-06-28 14:34:05', 2, '6a50e1ee1e0798d21ed123a800a31535'),
(765, 1, 'Bisch du e Möisebussard ?', '', 39, 0, NULL, '2013-07-03 09:34:22', 2, 'b23d8dbffedd8a7e6ea39a4a8d34dede'),
(795, 2, 'a794b73f7315cdc8fa2d7fabfa232b53', '', 49, 0, '2014-02-17 13:35:08', '2013-08-06 08:23:19', 2, 'f9eab30a93faafaa70271bebef8b21f0'),
(799, 1, 'Alexander Kmeth', 'Ohne Worte!', 6, 0, NULL, '2013-09-12 15:14:29', 2, '536531dcb7765c4618e8745cb46f59a3'),
(800, 1, 'Grüezi Frau Herrgott,\nBitte um Beachtung des Kommentars von Herr Hulliger. \n', 'Danke und freundliche Grüsse \nE. Griesser', 6, 0, NULL, '2013-09-13 13:41:58', 2, '3cb86a1a02ac8773af50c1444e84ae72'),
(801, 1, 'In AX 2009 stürzt de dr Batch nümme ab...', 'Usred für Trännu Batch Problem.', 6, 0, '2013-09-19 11:26:19', '2013-09-13 13:43:00', 2, 'b13e1ff595081b1081f2510bb66c8162'),
(803, 1, 'Ja, das ist ein strategischer Kunde!', '- Ruedi', 6, 0, NULL, '2013-09-26 07:43:20', 2, '6b840a79351ade4d75c5f243b3a09d7f'),
(804, 2, 'd0b651ef26f6a0d165828f4170005821', NULL, 51, 0, NULL, '2013-09-26 08:02:48', 2, '481d6dfb0735c66a1409b83c617b74ea'),
(805, 1, 'sexy', '', 50, 0, NULL, '2013-09-26 08:23:06', 2, '70abe1b4de8e9d62a86b2303d3bbe545'),
(806, 1, 'Entwicklungs-Standards von Seiten PTV SWISS gibt es keine', 'Verbatim aus FLO\'s IPA', 49, 0, '2013-10-21 12:07:41', '2013-10-09 12:59:13', 2, 'cb99d826cf4520e23dc02b533462b00b'),
(811, 1, 'I schicke der es Mail FLY', 'Mi Chef. Är meint FYI het\'s aber gloubs nid verstande...', 8, 0, '2014-01-15 14:04:04', '2014-01-10 21:17:50', 2, '4edd009f56778f18dcbcac195a26f386'),
(812, 2, '7b6fb0d1d1ee7bb706e4bcfed42c6ec7', NULL, 14, 0, NULL, '2014-02-03 11:52:47', 2, 'a56a90060d5c55706d7c1ec919566831'),
(813, 2, 'c96e5414acb9fc8a4fa72da1452817f5', NULL, 6, 0, NULL, '2014-02-06 14:55:10', 2, '15e845903b17a6d92d164a569c6f71fe'),
(814, 2, '508a38298a221852e59b572d045e9ba0', NULL, 8, 0, NULL, '2014-02-07 15:06:54', 2, '799b1dbb567c613c9dd5d6506fa45f2d'),
(815, 2, '3035f23545ee6688afd306c21c3f4a24', NULL, 8, 0, NULL, '2014-02-17 00:07:09', 2, 'ac7d0e49f1a8bf98815669997c7f1466'),
(816, 2, '66db792297b30d7cd01a43391664c89e', NULL, 8, 0, NULL, '2014-02-17 00:08:04', 2, '0a012a90f26eedd592b1e121f26f63d4'),
(817, 2, '778f4ce8e3838ef7c721bff9d07ac593', NULL, 8, 0, NULL, '2014-02-17 00:09:19', 2, 'bb11e4c9f25e57ae7f2e09dda3c9cbae'),
(818, 2, 'd9ce1f00d6f5cec9145bf37628fa36a9', NULL, 14, 0, NULL, '2014-02-24 11:29:57', 2, '0dff29565ecc5b2739173f3f1aaf5bfd'),
(819, 1, 'Wenis nid versta ischs nid guet.', '- Ruedi', 49, 0, '2014-02-28 17:29:07', '2014-02-28 15:26:41', 2, '9e70093dda11617752359dadd97a6731'),
(820, 1, 'Googel ist in der klaud geboren und darum wolkig leicht!', '- Ruedi', 49, 0, NULL, '2014-05-12 12:10:52', 2, '254b8648fa4b3ae6bf9c8fb3723a4ee5'),
(821, 2, '9b92b175c9b91e7b83070033f7635e13', NULL, 14, 0, NULL, '2014-05-19 11:29:53', 2, '0693fdd98843981b01c9e2a66cf8b1f1'),
(822, 2, '0ba814d5ba6c194b8f3de4b54f4ad861', NULL, 6, 0, NULL, '2014-05-26 12:12:50', 2, 'd1e6867a56b647fa6ba3b6ff7d5a6ce4'),
(823, 1, 'Jakes Priorit&amp;auml;ten', '[11:48:12] Jacob Brunner, Xplanis AG: essen ist heute nicht besonders hoch auf meiner prio-liste :) sieht etwa so aus:\n1.) arbeiten (homeoffice)\n2.) weinen mit optionalem stcklihzsten\n3.) sterben\n4.) sex\n5.) siehe 4\n6.) siehe 4\n7.) mehr weinen\n8.) essen', 49, 0, '2014-09-22 13:43:00', '2014-09-22 09:57:49', 2, '9d10107ad25da4dbe7d7ed96535efc81'),
(827, 1, 'Yes, we need to disc&amp;ouml;sh', 'engl. f&amp;uuml;r mir luege denne', 49, 0, '2015-09-25 12:30:16', '2015-09-25 10:29:10', 2, 'a14bbe7aa18ca727c5729f8f19eb4a0b'),
(828, 1, 'Eine &quot;Mike or Buy&quot; Analyse', '', 14, 0, '2015-09-25 13:43:18', '2015-09-25 11:42:55', 2, '06dd336a0b5066e0013b6b1060e5f671'),
(829, 1, 'Klammer nicht zu', '', 49, 0, NULL, '2015-10-01 07:41:02', 2, '9e15ad9d6e1d41baf32be58a72695a82');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Entry`
--
ALTER TABLE `Entry`
  ADD PRIMARY KEY (`EntryID`),
  ADD UNIQUE KEY `RecId` (`RecId`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `DataAreaID` (`DataAreaID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Entry`
--
ALTER TABLE `Entry`
  MODIFY `EntryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=830;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
