-- phpMyAdmin SQL Dump 
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Server-Version: 5.5.52-MariaDB
-- PHP-Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ISTD`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `EntryComments`
--

CREATE TABLE `EntryComments` (
  `CommentID` int(11) NOT NULL,
  `CommentCont` varchar(255) NOT NULL,
  `EntryID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `UpdateTimetime` datetime DEFAULT NULL,
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DataAreaID` int(11) NOT NULL,
  `RecId` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `EntryComments`
--

INSERT INTO `EntryComments` (`CommentID`, `CommentCont`, `EntryID`, `UserID`, `Status`, `UpdateTimetime`, `CreationTimestamp`, `DataAreaID`, `RecId`) VALUES
(52, 'Ooooh jaah!!!', 502, 6, 0, NULL, '2012-01-04 22:43:00', 2, 'b6ed0fb6053cda3a5d15892dd6d2fb91'),
(53, 'Nei es isch LP-Köbi!', 518, 6, 0, NULL, '2012-01-07 14:27:00', 2, 'c9ce8c8f4a9412750d3d9783d0a5d8ed'),
(54, 'I weiss aber der Flo hets so wöue', 518, 8, 0, NULL, '2012-01-05 20:13:00', 2, '4de1121cb947e66ae5cbc695e6c66e94'),
(55, 'Es isch LP-Andi', 518, 9, 0, NULL, '2012-01-05 18:23:00', 2, 'c733f9956c1edb5406d77de680c3bbee'),
(56, 'Basils BSH het g\'failet. Doch dr Andra het mit eirä Zilä Code repariert!', 519, 6, 0, NULL, '2012-01-06 06:16:00', 2, '6f5052aa4e87ce646b58462369ec46bb'),
(57, 'Dr Ändu. Ih verstahs nid!', 524, 6, 0, NULL, '2012-01-05 15:18:00', 2, '582dfbc8ef296a970684fe912c6f9021'),
(58, 'Giséle Recordon', 529, 8, 0, NULL, '2012-01-05 15:28:00', 2, 'bf7b78a00b92b5c16a404d9c9bedfc3b'),
(59, 'Learn to plan', 530, 8, 0, NULL, '2012-01-05 15:27:00', 2, 'b77efe37cccc3df6dda8bc90f177aca6'),
(60, 'Är geit doch nur bis 6,7 ;-)', 536, 8, 0, NULL, '2012-01-07 21:36:00', 2, '504d451c3ba3ec2ae3645b8afa33b24b'),
(61, 'Epic Fail\r\nObwou ärs no fascht het gschafft!', 539, 6, 0, NULL, '2012-01-06 06:09:00', 2, '117e2f2d7f62e2dbf0e73ddd4992c8e3'),
(62, 'Episch', 540, 8, 0, NULL, '2012-01-05 20:14:00', 2, 'b955d221dc47099ce00f570631fac3e0'),
(63, 'Haha, dr BO Drachä', 544, 6, 0, NULL, '2012-01-07 14:28:00', 2, '461bfb7f867bccbcae800bc99acb741e'),
(64, 'Das isch am Vitality-Day vor ptv gsi... Mir si im car am pokere gsi und d\'Nina seit statt All-In Full-In.', 544, 8, 0, NULL, '2012-01-07 07:54:00', 2, '9d2a6d5ca6e29f8e7ad6066e5e299378'),
(65, 'Ih ha ke Plan! ;-)', 544, 6, 0, NULL, '2012-01-06 05:22:00', 2, 'c36913630b3d61866fa66683a9b8c763'),
(66, 'Isch scho chli euter weiss nid ob der dä kennet... Der Jake sicher', 544, 8, 0, NULL, '2012-01-05 20:11:00', 2, '170a64fdb310ce1d0e92392b6687ec90'),
(67, 'LOL', 548, 14, 0, NULL, '2012-01-15 12:02:00', 2, 'db04e4662f9cf3fb3635efc2286ea13b'),
(68, 'Mi per het sech grad e fiat kouft dört chasch das o....', 550, 8, 0, NULL, '2012-01-07 21:15:00', 2, '3a82cfdea63480bd9af0d9657732f759'),
(69, 'C -> C++ -> C# -> C Herr Baly', 563, 15, 0, NULL, '2012-01-10 10:14:00', 2, '780ae58bcd32202d8466a58642314fba'),
(70, 'Gandi=Grantigä-Andi', 571, 6, 0, NULL, '2012-01-10 21:59:00', 2, 'd662f355eaa66ed0b72086d534307545'),
(71, 'Looserandi\r\nWinnerandi\r\nGängschterandi', 571, 8, 0, NULL, '2012-01-10 11:45:00', 2, '72128c7036a6daa11c9ea4549ba990d1'),
(72, 'Weni dä mau gseh, geit er druf! xD', 581, 6, 0, NULL, '2012-01-11 09:29:00', 2, 'ad29d6476b9a77c04abdc2a91f79f3ac'),
(73, 'Wie Floh immer sagt', 583, 14, 0, NULL, '2012-01-12 05:19:00', 2, 'bbac1f6a8ad3934789252f2c331b74a8'),
(74, 'Nein. Bei der Bitziusstrasse hat ein Typ das PTV Schild mit "Atomics" getagt', 584, 6, 0, NULL, '2012-01-22 02:30:00', 2, '7a76346c8258047541427a39b648c043'),
(75, 'Sind deinen Ski?', 584, 14, 0, NULL, '2012-01-12 05:18:00', 2, 'b5bfe2d76bc9dc8bc9caf6a9ed80b957'),
(76, 'Oder auch\r\nGira und Tschira', 590, 14, 0, NULL, '2012-01-15 12:01:00', 2, '03103ec230f7d62173681fd686242ab7'),
(77, 'Das muesch nid gseh ha, das muesch ghört ha...', 591, 8, 0, NULL, '2012-01-22 14:00:00', 2, '78a90d227cc5d7b138e54fb204720ead'),
(78, 'Fuck, hätti huere gärn gseh!!!', 591, 6, 0, NULL, '2012-01-22 02:35:00', 2, '67d555f17a3d624fd248d025a778b347'),
(79, 'Ach so', 594, 14, 0, NULL, '2012-01-18 19:36:00', 2, '796d8364ca892f1cb8b3a1c95b9945e0'),
(80, 'Ah dä Dealer im Cäsu isch eifach nume Pflicht!', 596, 6, 0, NULL, '2012-01-20 17:25:00', 2, 'd841c82254f3a2070e4d92b6801de6bc'),
(81, 'winner winner chicken dinner money boy swagger rap', 596, 6, 0, NULL, '2012-01-19 22:55:00', 2, 'dbb488ac2ce16cd1a183085493aeadc2'),
(82, 'Ungeschlagen im sig, sag, sug i fahre = Andrea', 597, 8, 0, NULL, '2012-01-22 13:59:00', 2, '1ffef19cc0f008ead17ff2fad3ccc51e'),
(83, 'Oooh Jah! Ih fahrä!!!', 597, 6, 0, NULL, '2012-01-22 02:30:00', 2, '4205401a55aa0bf0b540f61132c1a9ab'),
(84, 'LOL!! Typisch von SPOC Lüüt', 599, 14, 0, NULL, '2012-01-20 17:56:00', 2, '52df0edd927d6aac00bd3c65ab81a247'),
(85, 'Ah, wenn wir etwas unangebrachtes gesagt haben?!?\r\nEpisch', 601, 6, 0, NULL, '2012-01-21 19:22:00', 2, '065652f869f2ee9c0ccb2dce703355bc'),
(86, 'Und die varianten:\r\nSeba, Bitte!\r\nJake, Bitte!\r\nAndrea, Bitte!\r\n...', 601, 14, 0, NULL, '2012-01-21 14:47:00', 2, '1f35e1f2ea6d4f1d305d0b4e1cc9e7cf'),
(87, 'Kantonspolizei Bern, Moser', 603, 8, 0, NULL, '2012-01-28 20:01:00', 2, '0babfd635244e38f33a93e958419186b'),
(88, 'Chills mau!', 605, 6, 0, NULL, '2012-01-22 22:01:00', 2, 'be2ea43fa1b61637127b8e3e81a3767b'),
(89, 'Lol', 606, 14, 0, NULL, '2012-01-24 05:24:00', 2, '847e62341652122dbc5c24a88ebbee03'),
(90, 'PTV Swiss..... Age Schmid', 606, 14, 0, NULL, '2012-02-25 05:51:00', 2, 'adca71de36ec904f85ec1c3f65e9846c'),
(91, 'es isch blick am abend...', 607, 9, 0, NULL, '2012-01-23 18:11:00', 2, 'd577d52e78fd961dcdd4c4c83b8ebd83'),
(92, 'Nice', 609, 14, 0, NULL, '2012-01-24 05:24:00', 2, 'f373597da2ac286396797838ed9b3b70'),
(93, 'Ges- Torben', 612, 14, 0, NULL, '2012-01-24 20:05:00', 2, '2285d62c4afac769737bd68e32a9fe98'),
(94, '-Sone Löiier!\r\n-Löiig', 613, 6, 0, NULL, '2012-01-24 20:12:00', 2, '982a90f1c3eaa2a135954b3f5373973f'),
(95, 'Word!', 638, 14, 0, NULL, '2012-01-30 05:32:00', 2, '1e0d05daf41d6bd75ebdca4f13ea86dd'),
(96, 'Haha! Episch!', 642, 6, 0, NULL, '2012-01-31 10:10:00', 2, 'a9718f796ab09aee6957ed3231be4e76'),
(97, 'Venesvanner', 661, 8, 0, NULL, '2012-02-06 22:29:00', 2, 'def4f42525d7f860e474ea266badec71'),
(98, 'Aaaah. Es ist immer so schön warm!', 663, 6, 0, NULL, '2012-02-09 12:01:00', 2, '48594a0a3bf8afcf839f5b4c2f565fd4'),
(99, 'Bei secure post auch! ;-)', 663, 14, 0, NULL, '2012-02-09 14:45:00', 2, 'af83445f7287bd8cda544377340ebc0a'),
(100, 'Oder:\r\n-Pötti Mc Ött\r\n-Ötti Mc Pött', 667, 6, 0, NULL, '2012-02-14 13:00:00', 2, '9ba0bb7003a5d0f7ad1e8fece22b8f1e'),
(101, 'Ist der Ottiger von CBB?:-)', 667, 14, 0, NULL, '2012-02-19 14:53:00', 2, '093c9cc7f7512423f4594c38f35220e4'),
(102, 'Haha. Nein. xD\r\nEs ist ein Aufruf zum rauchen.', 667, 6, 0, NULL, '2012-02-19 16:00:00', 2, 'd14bb0c9a5083ee2325fed2b56b35108'),
(103, 'So viu Episch uf einisch... ;-)', 669, 6, 0, NULL, '2012-03-06 15:34:00', 2, 'a93735471eb43c7c81d5e9add1d0f1b0'),
(104, 'Email von Judith an Seba, nachdem er ein Jira kommentiert hat.\r\nOhne begrussung', 669, 14, 0, NULL, '2012-03-06 15:35:00', 2, 'a34a0d9266d74db4300357878d6a5433'),
(105, '1 Wiche speter.... Keilrieme grisse', 670, 8, 0, NULL, '2012-04-06 16:59:00', 2, '24eefad8a15ff906aecd7407cd1c7597'),
(106, 'Aaahh ja, jetzt weisi wo... xD', 671, 6, 0, NULL, '2012-05-11 09:35:00', 2, 'd1fcd059a35bc8123147ec5906f31f17'),
(107, 'Stimmt doch ', 678, 49, 0, NULL, '2013-04-18 12:38:00', 2, '8581a7ab5c3fd426408f0cec6dd882b0'),
(108, 'Gueti Idee! Aaah nei...', 680, 6, 0, NULL, '2013-04-16 18:30:00', 2, '7dbb7a41097102f2c2f8c039d63ecae5'),
(109, 'Die beschti Idee ds Läder isch när wie Anke', 680, 8, 0, NULL, '2013-04-19 15:36:00', 2, '551da303ea877cc9453103450d7ed736'),
(110, 'Isch klar... ;-)', 680, 6, 0, NULL, '2013-04-19 16:10:00', 2, 'fba8a5f2fa6df29eb416a4f82aa66121'),
(114, 'Usfüehre, marsch', 698, 8, 0, NULL, '2013-05-06 07:41:52', 2, '0b45672fffe2656d38bf3e22fa372303'),
(125, 'TOP !', 700, 39, 0, NULL, '2013-05-07 09:03:28', 2, 'eca0142905cb42c073c884c652dac7db'),
(126, 'Ahahah! Ist es Judiths Sohn, gau?', 701, 14, 0, NULL, '2013-05-13 16:06:01', 2, 'f133c63619e88bbc16cd25e0db40e16a'),
(127, 'LOL! Wer war der Künstler?', 724, 14, 0, NULL, '2013-05-13 16:08:16', 2, '095f5f0ce0ecb56ddadb18af7581dd29'),
(128, 'Dies hat Elia einmal im Paint erstellt als Beispiel für ein neues Xplanis Logo...', 724, 6, 0, NULL, '2013-05-13 16:24:49', 2, 'da22e8fb4efd465e534ca51bf8c7f3aa'),
(129, 'Haha, Jah ;-)', 701, 6, 0, NULL, '2013-05-13 16:29:24', 2, '7b7343a67e6d889964d4873ca4eb2ae2'),
(134, 'niemand weiss wie silvia das geschafft hat... danke.bmp', 756, 49, 0, NULL, '2013-05-29 13:54:33', 2, '9ffa659484e783a8ecf9605094c93ac0'),
(135, '????????BA?', 712, 49, 0, NULL, '2013-06-11 12:13:49', 2, 'b63009cb256e5e6586dada588c3d46af'),
(136, 'Scheiss Konami Code', 712, 6, 0, NULL, '2013-06-11 12:39:53', 2, 'b37c2821ad1816f1eadc40a258f563a9'),
(137, '&quot;Fele la mis form la si&quot;\nwi wärs mit nei?', 760, 49, 0, NULL, '2013-06-25 12:52:47', 2, 'f66cf1dd8fd31b1347467c125bd19189'),
(138, 'Jedes mau!', 761, 6, 0, NULL, '2013-06-28 09:02:27', 2, '264fd5047360d2b717d71a60faed13a7'),
(139, 'Glas wurde nicht versiegelt...', 662, 8, 0, NULL, '2013-06-28 09:09:19', 2, '250ed10b5a388077d19c7984c583cffd'),
(147, 'Typisch Dütsch!', 799, 14, 0, NULL, '2013-09-13 06:39:35', 2, 'd50bcf67a4cfa5d36197993fca9dbe6e'),
(148, 'Von der Firma Peschtalozzi und Ko.age :D\n', 800, 14, 0, NULL, '2013-09-13 14:53:43', 2, '23b88f2cf7796ea4aae107a9f1d46ebf'),
(149, 'Der ottiger isch scho nid schlächt...', 679, 8, 0, NULL, '2013-09-20 19:27:22', 2, 'e4b708b11c8e567b0cb0474b90cea42e'),
(150, 'Übrigens seit das mi nöi chef o... ;)', 542, 8, 0, NULL, '2013-09-20 19:33:19', 2, '440891c3f0418a1d9d654e26109e0bcb'),
(151, 'Soooo episch!', 762, 8, 0, NULL, '2013-09-20 19:44:11', 2, 'ab70e6c4582fc835a3be19ec594a079c'),
(152, 'Der Denker', 700, 8, 0, NULL, '2013-09-20 19:45:28', 2, 'c4facc3d814a846e22cf6ee21ba563b3'),
(153, 'Der Kiffer', 804, 51, 0, NULL, '2013-09-26 08:07:05', 2, '50622c2ad62bdd2c59216f1ce1c7b501'),
(154, 'Banane ?', 763, 39, 0, NULL, '2013-09-26 10:13:07', 2, 'e2cf3dacbd039dd6c6df8f25684cae9a'),
(155, 'Welcher Kunde ist das nicht??', 803, 8, 0, NULL, '2013-10-19 11:41:21', 2, '070816f7b897a6e1bf5389b969d31ad3'),
(156, 'Weitere Produkte aus dem Hause Ruedi:\nLPRoute\nDimaDispatch\nRouteLogistik\nTourKomm\neClusterPlatform\nStrategieSmart\nPTVKommCluster\nStandardPlatform\nStandardLogistik\nNaviMarket\nLogistikCluster\nPlatformStandort\nMarketPlatform\n', 741, 49, 1, NULL, '2013-11-04 09:36:29', 2, 'c80047d9bf8a5ab20cc14696b9f4ddff'),
(157, 'Weitere produkte:\nLPRoute, DimaDispatch, RouteLogistik, TourKomm, eClusterPlatform, StrategieSmart, PTVKommCluster, StandardPlatform, StandardLogistik, NaviMarket, LogistikCluster, PlatformStandort, MarketPlatform, CargoDima, OrderPlatform, xSalesStrategi', 741, 49, 1, NULL, '2013-11-04 09:38:43', 2, '66fe8b38b325c8013a9f4cc4d4cb656a'),
(158, 'Pestasupport griesser vor pestalozzi vom support vor ag griesser?', 800, 49, 0, NULL, '2013-11-15 08:25:25', 2, 'facc0ea3dc7c9391a9379fbeea4c5272'),
(159, '????????BA\n\nimmerno kaputt', 712, 49, 0, NULL, '2013-11-15 08:26:42', 2, 'd9e03e232460553fdd2dfbe582b314d4'),
(160, 'Verstani nid?!?', 811, 6, 1, NULL, '2014-01-13 10:03:49', 2, '88766ea45785669b6a03fbee750d522f'),
(161, '&gt; Är meint FIY\nnid eher FYI? :D', 811, 49, 1, NULL, '2014-01-13 10:13:53', 2, '183e6737267d5b099c744fa0ebb7c014'),
(162, 'Epische Email von Sebastian Wehowski!\n\nSollte noch jemand auf der Liste fehlen, z.B. der Hausmeister, :DDD\n', 812, 14, 0, NULL, '2014-02-03 11:54:28', 2, '3753e2c8197cd788434b7a6c42f05739'),
(163, 'Seba so guuuut!!!! S U P E R!!! ', 812, 51, 0, NULL, '2014-02-03 12:05:43', 2, '156ab5843d037c950c21bd59db7b6c9c'),
(164, 'wow :D', 812, 49, 1, NULL, '2014-02-03 12:06:16', 2, '1fddb09f77ee155df5a9ad10ecfe9b17'),
(165, '&gt; gerne eine mail an mich oder auch an alle\nIch kann nicht mehr ?', 812, 49, 0, NULL, '2014-02-03 12:15:07', 2, '519ed7a9b781a89eb37285db0ad391cb'),
(166, 'soo episch :\'D', 795, 9, 0, NULL, '2014-02-06 16:07:54', 2, '7d76887526aafd7053e7c4be6d4c62f7'),
(167, 'Offizielles Textbeispiel auf der Post Homepage für den Muttertag....', 814, 8, 0, NULL, '2014-02-07 15:07:58', 2, '2f6a4f05bddac84493240b94835b33a4'),
(168, 'Calculation Win', 816, 6, 0, NULL, '2014-02-17 07:39:44', 2, '4f0834074f815a5f1bdd6dfd950d23d4'),
(169, 'Durchschnittliche xPlanis qualität', 813, 49, 0, NULL, '2014-02-17 11:28:43', 2, 'debcccdc546743acb676e6461b74e786'),
(170, 'Du bist Meister!', 816, 14, 0, NULL, '2014-02-18 07:59:03', 2, '63b01056b792767d0ffb2d343c9d1e13'),
(171, 'Pestalozzi und Co. A-Ge!', 818, 14, 0, NULL, '2014-02-24 11:30:43', 2, '80e7108f29e559c3ca9c8ec6fea10aa0'),
(172, '', 821, 14, 1, NULL, '2014-05-19 11:31:30', 2, 'd9ac4d85c95886ef0990a4d725585385'),
(173, '', 821, 49, 1, NULL, '2014-05-19 11:50:07', 2, '5331e96aa2ea64a88013a58ae5a535f6'),
(174, 'Jira Fail. WTF', 822, 6, 0, NULL, '2014-05-26 12:13:14', 2, 'daa1b8acef517b6ad67bb5153de9eac2'),
(175, 'Fabian ist seit Lange in die PTV... :-D\n', 822, 14, 0, NULL, '2014-05-26 12:20:36', 2, 'faa4c974564d527bc9503210081ddc08'),
(176, 'Interne Kommunikation von Kunden in unserem Jira. :-D\n', 821, 14, 0, NULL, '2014-05-26 12:22:29', 2, '81ec26ff10747de1bd329fae0b245ab2'),
(177, 'nidemal 13:37... sone fake!!', 822, 49, 0, NULL, '2014-08-29 07:40:16', 2, '8c1af2b5cf76dbe2efd5330859ca8013');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `EntryComments`
--
ALTER TABLE `EntryComments`
  ADD PRIMARY KEY (`CommentID`),
  ADD UNIQUE KEY `RecId` (`RecId`),
  ADD KEY `EntryID` (`EntryID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `DataAreaID` (`DataAreaID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `EntryComments`
--
ALTER TABLE `EntryComments`
  MODIFY `CommentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
