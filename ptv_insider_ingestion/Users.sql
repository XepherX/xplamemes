-- phpMyAdmin SQL Dump 
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Server-Version: 5.5.52-MariaDB
-- PHP-Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ISTD`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Users`
--

CREATE TABLE `Users` (
  `UserID` int(11) NOT NULL,
  `EMail` varchar(255) NOT NULL,
  `Passwort` varchar(255) DEFAULT NULL,
  `Firstname` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `Token` varchar(255) NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `SID_UserID` varchar(40) DEFAULT NULL,
  `RegisterDate` datetime NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '2',
  `SendMail` int(11) NOT NULL DEFAULT '0',
  `CreationTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DataAreaID` int(11) NOT NULL,
  `RecId` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Users`
--

INSERT INTO `Users` (`UserID`, `EMail`, `Passwort`, `Firstname`, `Lastname`, `Token`, `LastLogin`, `SID_UserID`, `RegisterDate`, `Status`, `SendMail`, `CreationTimestamp`, `DataAreaID`, `RecId`) VALUES
(50, 'mschlapbach@gmail.com', '', 'Matthias', 'Schlapbach', '34b61a5024efbcdc69cb6efc2ed61187', '2015-09-25 12:33:09', '897200cbb2657be50534752033743583', '2013-06-26 21:05:05', 2, 0, '2013-06-26 19:05:05', 2, '6e615c1f8a1f05fa5d8a39df33046c82'),
(6, 'florian.hulliger@gmail.com', '', 'Florian', 'Hulliger', '2163dd9a466dc2e50c4595a7f7f4fe72', '2016-03-21 21:47:58', 'd7a2d4f8b426015716145c75dcd47d16', '2012-01-04 23:27:00', 1, 0, '2012-01-04 22:27:00', 2, 'e56e984251b49ea6a7c0728411555f51'),
(8, 'andreamarrazzo@hotmail.com', '', 'Andrea', 'Marrazzo', 'f8ce9511f5d5d871716820d047335e89', '2016-02-06 22:02:34', '7c28acbb85586172dc6346f01308849d', '2012-01-05 14:09:00', 2, 0, '2012-01-05 13:09:00', 2, 'fed729fa71c6703958819ec9cdc98652'),
(9, 'elia.delfavero@gmail.com', '', 'Elia', 'Del Favero', '1a92468ce0ca524fefbf4face8f5bc23', '2014-09-22 11:58:44', '824a75483adff47869367cf2216328e7', '2012-01-05 15:48:00', 2, 0, '2012-01-05 14:48:00', 2, '47ad7dde351391654ed064e049836f45'),
(14, 'sebastiano.columbano@ptvswiss.ch', '', 'Sebastiano', 'Columbano', 'ffdc9193235bf8836123bd2a0c868536', '2015-10-01 11:48:44', '18ce8559d88c78647607723d809397c3', '2012-01-09 10:48:00', 2, 0, '2012-01-09 09:48:00', 2, '9ec88fd365e00e67a26cf1843361d69e'),
(15, 'jacob.brunner@ptvswiss.ch', '', 'Jacob', 'Brunner', 'ea3cc94cae316f66e6388f52a4dbb21a', '2013-05-06 10:16:20', '93ec214f7fe36f0f6f6f2560eeafd4be', '2011-12-14 10:58:00', 2, 1, '2011-12-14 09:58:00', 2, '8e4e6dbf99a6252aed786d82d2500f92'),
(38, 'ronny.beck@gmx.ch', '', 'Ronny', 'Beck', '01744c853d8b1c225a0339350c665d5f', '2013-05-06 15:54:55', 'aaea696f8c17ee83aa0a6b62574ca578', '2012-02-02 10:30:00', 2, 1, '2012-02-02 09:30:00', 2, '17e607be304ce32b8a19af0f2cc321de'),
(39, 'b-asil@gmx.ch', '', 'Basil', 'Rakasz', '0sa54f5asg4sag0sa56g40sa5g', '2014-05-13 00:19:50', 'f2b192a661e5cd26ccb8a1c1237aa246', '2012-02-06 14:05:00', 2, 0, '2012-02-06 13:05:00', 2, 'a8155b324d6f28fbe728634a5b4d3e2a'),
(49, 'ff@xplanis.com', '', 'Fabian', 'Feller', '42a9adadfa086b98f88f7fbde8c81361', '2016-02-22 16:19:00', '6e1d95e77628c88fbbef349b48bc3600', '2013-04-18 14:56:00', 2, 1, '2013-04-18 12:56:00', 2, '3596acc65e89df9172eb28af66da89e3'),
(51, 'ho@xplanis.com', '', 'Hasan', 'Oda', '0492832202f351aabc9182a38a4a420b', '2014-09-22 14:18:40', 'b2865944e6a2932640ea13ebc2f9ae8c', '2013-09-26 09:45:31', 2, 0, '2013-09-26 07:45:31', 2, '15745a1b2a07e02d7b83d4d2d0b03632'),
(52, 'pb@xplanis.com', '', 'Pascal', 'Blaser', '36ed83e789e95e37e11879470e61d923', '2015-09-25 14:58:15', 'b33c59c390a4a516e52084cc51c76084', '2015-09-25 12:35:08', 2, 0, '2015-09-25 10:35:08', 2, 'f5a942919af623f33d6b145ba9f41f1e'),
(53, 'fabian.feller@xplanis.com', '', 'F', 'F', '55fd20fcc0662bc439442a82fff3dc79', '2016-01-12 12:51:38', '74e9b539c4ba1f9fdb46f4018aeab44d', '2016-01-12 12:47:09', 2, 0, '2016-01-12 11:47:09', 2, 'ccc351790ee4a2bfcd9bb983be071691');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `Token` (`Token`),
  ADD UNIQUE KEY `RecId` (`RecId`),
  ADD UNIQUE KEY `EMailDataAreaID` (`EMail`,`DataAreaID`),
  ADD UNIQUE KEY `SID_UserID` (`SID_UserID`),
  ADD KEY `DataAreaID` (`DataAreaID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Users`
--
ALTER TABLE `Users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
