require("dotenv").config();
var model = require("../models/model.js");
var csv = require("csv");
var fs = require("fs");
var q = require("q");
var glob = require("glob");
var path = require("path");
var Guid = require('guid')
//columns: ['int', 'string', 'string', 'string', 'string', 'string', 'string', 'int', 'int', 'string', 'int', 'string'] 

var users = fs.readFileSync("user.csv", "utf8");
var entries = fs.readFileSync("entry.csv", "utf8");
var comments = fs.readFileSync("entrycomment.csv", "utf8");
var images = fs.readFileSync("images.csv", "utf8");
var imageComments = fs.readFileSync("imagecomment.csv", "utf8");


var clearDb = function () {
    return q.all([model.User.remove({}), model.Quote.remove({}), model.Comment.remove({}), model.Image.remove({})])
}
var parseUsers = function () {
    var promise = q.defer()
    csv.parse(users, { columns: true, delimiter: ";", auto_parse: true }, function (err, out) {
        promise.resolve(out);
    });
    return promise.promise;
}

var parseQuotes = function () {
    var promise = q.defer()
    csv.parse(entries, { columns: true, delimiter: ";", auto_parse: true }, function (err, out) {
        promise.resolve(out);
    });
    return promise.promise;
}
var global = this;
global.userIdMap = {};
var importUsers = function () {

    var importUserPromises = parseUsers().then(function (rows) {
        console.log("importing " + rows.length + " users")
        var promises = [];
        for (var i = 0; i < rows.length; i++) {

            var a = function (row) {
                var promise = q.defer();
                var that = this;

                var user = new model.User({
                    email: row.Email,
                    username: row.Vorname + " " + row.Name
                });
                promises.push(promise.promise);
                user.save(function (err, savedRecord) {
                    if (err)
                        promise.reject(err);
                    global.userIdMap[row.ID] = { id: savedRecord._id, name: savedRecord.username };
                    promise.resolve(global.userIdMap);

                });
            }(rows[i]);
        }
        return promises;
    });

    return q.all(importUserPromises);
}

global.quoteIdMap = {};
var importQuotes = function () {
    return parseQuotes().then(function (rows) {
        console.log("importing " + rows.length + " quotes")
        var promises = [];
        for (var i = 0; i < rows.length; i++) {

            var closure = function (row) {
                var promise = q.defer();
                var quote = new model.Quote({
                    submitter: global.userIdMap[row.SubmitterID].id,
                    timestamp: row.TimeStamp,
                    title: row.Content.replace(/\\r\\n/g, "\r\n").replace(/\\n/g, "\n").replace(/\\r/g, "\r"),
                    content: row.Description.replace(/\\r\\n/g, "\r\n").replace(/\\n/g, "\n").replace(/\\r/g, "\r")
                })
                quote.save(function (err, savedRecord) {
                    if (err) {
                        return promise.reject(err);
                    } else {
                        global.quoteIdMap[row.ID] = savedRecord._id;
                        return promise.resolve();
                    }
                });
                promises.push(promise.promise);
            }(rows[i]);


            var oldRecord = rows[i];
            //console.log("matched " + oldRecord.SubmitterID + " to " + userIdMap[oldRecord.SubmitterID])
        }
        return promises;
    })
}

var parseComments = function () {
    var promise = q.defer()
    csv.parse(comments, { columns: true, delimiter: ";", auto_parse: true }, function (err, out) {
        if (err) {
            return promise.reject(err);
        } else {
            return promise.resolve(out);
        }

    });
    return promise.promise;
}
var importComments = function () {
    return parseComments()
        .fail((e) => {
            console.error(e);
            q.reject(e);
        })
        .then(function (rows) {
            if (!rows) {
                return q.reject("rows was undefined")
            }
            var promises = [];
            console.log("importing " + rows.length + " quote comments")
            for (var i = 0; i < rows.length; i++) {
                var closure = function (row) {
                    var promise = q.defer();

                    var comment = new model.Comment({
                        content: row.Comment.replace(/\\r\\n/g, "\r\n").replace(/\\n/g, "\n").replace(/\\r/g, "\r"),
                        submitter: global.userIdMap[row.Submitter].id,
                        timestamp: row.TimeStamp
                    });

                    var outerPromise = comment.save()
                        .fail(function (err) {
                            console.log(err);
                        }).then(function (savedRecord) {
                            if (!savedRecord) {
                                console.error("record not saved??? " + comment.content)
                            } else {
                                console.log("looking up " + row.PostID + "= " + global.quoteIdMap[row.PostID])

                                return model.Quote.count({ _id: global.quoteIdMap[row.PostID] }).then((count) => {
                                    if (count > 0) {
                                        return model.Quote.findOne({ _id: global.quoteIdMap[row.PostID] })
                                        .then(function (found) {
                                            if (!found) {
                                                return q.reject(row.PostID)
                                            }
    
                                            found.comments.push(savedRecord._id);
                                            return found.save(function (err) {
                                                if (err) {
                                                    promise.reject(err);
                                                    return err;
                                                } else {
                                                    promise.resolve();
                                                }
                                            });
                                        })
                                    } else {
                                        return model.Image.findOne({ _id: global.quoteIdMap[row.PostID] })
                                        .then(function (found) {
                                            if (!found) {
                                                return q.reject(row.PostID)
                                            }
    
                                            found.comments.push(savedRecord._id);
                                            return found.save(function (err) {
                                                if (err) {
                                                    promise.reject(err);
                                                    return err;
                                                } else {
                                                    promise.resolve();
                                                }
                                            });
                                        })
                                    }
                                })

                                
                            }
                        }).fail((err) => {
                            if (err) {
                                console.log("row not found: " + err);

                            }
                        })
                        .then(function () {
                            return q.resolve();
                        });
                    promises.push(outerPromise);
                    promises.push(promise.promise);
                }(rows[i]);
            }
            return promises;
        })
}

var parseImages = () => {
    var promise = q.defer();
    csv.parse(images, { columns: true, delimiter: ";", auto_parse: true }, function (err, out) {
        if (err) {
            return promise.reject(err);
        } else {
            return promise.resolve(out);
        }

    });
    return promise.promise;
}

global.imageIdMap = {};
let importImages = () => {

    return parseImages().fail((e) => {
        console.log("mu reject" + e)
        q.reject(e);
    }).then((rows) => {
        console.log("importing " + rows.length + " images");
        var allPromises = [];
        let existingFiles = glob.sync("../images/*");
        for (let i = 0; i < existingFiles.length; i++) {
            fs.unlinkSync(existingFiles[i]);
        }

        for (let i = 0; i < rows.length; i++) {
            let filename = rows[i].Content;
            let file = glob.sync("./uploads/" + filename + "*")[0];
            let fileExt = path.extname(file);
            let imageGuid = Guid.raw();
            fs.copyFileSync(file, "../images/" + imageGuid + fileExt);

            let image = new model.Image();
            image.title = rows[i].Description;
            image.filename = imageGuid + fileExt;
            image.timestamp = rows[i].TimeStamp;
            image.submitter = global.userIdMap[rows[i].SubmitterID].id;
            var p = image.save().fail((e) => {
                return q.reject(e);
            }).then((saved) => {
                console.log("adding " + rows[i].ID + " = " + saved._id)
                global.quoteIdMap[rows[i].ID] = saved._id
                return q.resolve();
            })
            console.log(p);
            console.log("puh")
            allPromises.push(p);

        }

        return allPromises;
    })
}



clearDb().fail((e) => {
    console.log(e);
}).then(() => {
    return q.all(importUsers());
}).fail((err) => {
    console.error(err);
}).then(() => {
    console.log("done")
    return q.all(importQuotes());
}).fail((err) => {
    console.error(err);
}).then(() => {
    console.log("done")
    var prom = importImages();
    console.log("PROMS ARE ")
    console.log(prom);
    return q.all(prom);
}).fail((e) => {
    console.error(e);
}).then(() => {
    console.log(global.quoteIdMap)
    console.log("done")
    return q.all(importComments());
}).fail((err) => {
    console.error(err);
}).then(() => {
    console.log("all done!");
    console.log();
    process.exit(0);
});



// clearDb().then(function () {
//     return q.all(importUsers());
// }).then(function () {
//     return q.all(importQuotes());
// }).fail(function (err) {
//     return;
//  })
// //.then(function () {
// //     return q.all(importComments());
// // }).fail(function (err) {
// //     return err;
// //     console.log("failed to import comments: " + err)

// // }).then(function () {
// //     console.log("SUCCESS");
// //     return;
// // }).then(function () {
// //     return;
// // })


