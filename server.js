require("dotenv").config();
var express = require('express');
var jwt = require('express-jwt');
var jwtCreator = require('jsonwebtoken');
var bodyParser = require('body-parser')
var password = require('password-hash-and-salt');
const fileUpload = require('express-fileupload');
var Guid = require('guid')
var path = require("path");
var thumb = require('node-thumbnail').thumb;
var compression = require('compression')
var request = require("request");
var q = require("q");

if (!process.env.JWT_SECRET)
	throw "No jwt secret"
var jwtSecret = process.env.JWT_SECRET
var model = require("./models/model.js");

var app = express();


if (!process.env.SERVER_PORT)
	throw "No port"

if (!process.env.MONGODB_URI)
	throw "No mongodb uri"

app.use(compression());
app.use(bodyParser.json());
app.use(fileUpload());

app.post('/login', function (req, res) {
	console.log(req.body)
	model.User.findOne({
		$or: [
			{ username: req.body.email },
			{ email: req.body.email }
		]
	}).then(function (user) {

		password(req.body.password).verifyAgainst(user.passwordHash, function (error, verified) {
			if (!verified) {
				console.log("nope");
				res.status(403)
				res.send({ error: "Verification failed" });
				return;
			} else {

				if (!user.isLoginAllowed) {
					res.status(403)
					res.send({ error: "User is not allowed to login" });
					res.end();
					return;
				}
				var token = jwtCreator.sign(
					{
						user: req.body.email,
						userId: user._id,
						isAdmin: user.isAdmin,
						isLoginAllowed: user.isLoginAllowed
					}, jwtSecret);
				res.send({ token: token });
				res.end();
				return;
			}
		})
	}).catch(function () {
		res.status(403)
		res.send({ error: "Verification failed" });
		res.end();
		return;
	})
});

app.post('/register', function (req, res) {
	console.log("registering user " + req.body.username)
	model.User.findOne({ username: req.body.username })
		.then(function (user) {
			console.log("query done, user is: " + user)
			if (user === null) {
				var createUser = new model.User();
				createUser.email = req.body.email;
				createUser.username = req.body.username;
				createUser.isLoginAllowed = true;
				var promise = q.defer();
				password(req.body.password).hash(function (error, hash) {
					if (error) {
						console.error(error);
						res.send(error);
						res.end();
						return;
					}
					console.log("creating user")
					createUser.passwordHash = hash;
					createUser.save().then((createdUser) => {
						promise.resolve(createdUser)
					});

				});
				return promise.promise;
			} else {
				res.send({ success: false, message: "user already exists" })
				res.end();
				return;
			}
		}).then((user) => {
			request.post('https://maker.ifttt.com/trigger/quotebase_user_signup/with/key/dJrvJymgT31E6sv4rWM4TN', {
				"json": {
					"value1": user.username,
					"value2": user.email
				}
			}, (err, res, body) => {
				console.log(err);
			});
			res.send({ success: true, message: "user created" })
			res.end();
			return;
		}).catch((e) => { console.err(e) });
});

app.post('/logout', function (req, res) {

});

app.use("/api/", function (req, res, next) {
	var bearer = req.get("authorization");
	if (bearer === undefined) throw "Unauthorized"
	var bearerToken = bearer.split(" ")[1];
	var parsedToken = jwtCreator.decode(bearerToken, { json: true, complete: true })
	model.User.find({ _id: parsedToken.payload.userId }).count().exec((err, count) => {
		if (count == 0) {
			res.status(401)
			res.send({ error: "Verification INTO THE TRAAAASH" });
			res.end();
		} else {
			next();
		}
	})


})

app.get("/api/users", jwt({ secret: jwtSecret }), function (req, res) {
	if (!req.user.isAdmin) {
		res.status(403)
		res.send({ error: "Admin only" });
		res.end();
		return;
	}

	model.User.findOne({ "_id": req.user.userId }).then((authorizedUser) => {
		console.log(authorizedUser)
		if (!authorizedUser.isAdmin === true) {
			res.status(403)
			res.send({ error: "Admin only" });
			res.end();
			return;
		}
	});

	model.User
		.find()
		.then(function (users) {
			res.send(users);
			res.end();
			return;
		})
});

app.put("/api/users/", jwt({ secret: jwtSecret }), function (req, res) {
	if (req.body && req.body.length > 0) {

		if (!req.user.isAdmin) {
			res.status(403)
			res.send({ error: "Admin only" });
			res.end();
			return;
		}



		for (var i = 0; i < req.body.length; i++) {

			var closure = function (user) {
				var existingUser = model.User.findOne({ "_id": user._id })
					.then(function (existingUser) {
						existingUser.isAdmin = user.isAdmin;
						existingUser.isLoginAllowed = user.isLoginAllowed;
						existingUser.save();
					})
					.catch(function (err) {
						res.status(400)
						res.send({ error: err });
						res.end();
						return;
					});
			}(req.body[i])

		}
	} else {
		res.status(400)
		res.send({ error: "expected array of users" });
		res.end();
		return;
	}

});

app.get('/api/quotes/', jwt({ secret: jwtSecret }), function (req, res) {

	var query = model.Quote

	if (req.query.pagination) {
		query = query.find({ _id: { $lt: req.query.pagination } });
	} else {
		query = query.find();
	}
	query.sort({ "timestamp": -1 })
		.limit(100)
		.then(function (quotes) {
			res.send(quotes);
			res.end();
			return;
		})
});

app.post('/api/quotes/', jwt({ secret: jwtSecret }), function (req, res) {

	var comment = new model.Quote({
		title: req.body.title,
		content: req.body.content,
		cited: req.body.cited,
		submitter: req.user.userId
	})

	comment.save().then(function () {
		res.send({ success: true });
		res.end();
		return;
	}).catch(function () {
		res.send({ success: false });
		res.end();
		return;
	});


})

app.get('/api/quotes/:id', jwt({ secret: jwtSecret }), function (req, res) {
	model.Quote
		.findOne({ _id: req.params.id })
		.populate("submitter", "username")
		.populate({
			path: "comments",
			options: {
				sort: {
					"timestamp": -1
				}
			},
			populate: {
				path: "submitter",
				select: "username"
			}
		})

		.exec(function (err, item) {
			res.send(item)
			res.end();
			return;
		})
});

app.post('/api/quotes/:id/comments', jwt({ secret: jwtSecret }), function (req, res) {
	console.log(req.params)
	var quoteId = req.params.id;

	var comment = new model.Comment({
		content: req.body.comment,
		quote: quoteId,
		submitter: req.user.userId
	});
	comment.save().then(function (createdComment) {
		return Promise.all([createdComment, model.Quote.findOne({ _id: quoteId })])
	}).then(function (array) {
		var createdComment = array[0];
		var relatedQuote = array[1];
		relatedQuote.comments.push(createdComment._id);
		return relatedQuote.save();
	}).catch(function () {
		res.status(500);
		res.end();
		next();
	}).then(function (t) {
		res.send();
		res.end();
		return;
	});

});

app.post('/api/images/', jwt({ secret: jwtSecret }), function (req, res) {
	if (!req.files) {
		return res.status(400).send('No files were uploaded.');
	}

	console.log(req.body);

	var file = req.files["file"];
	var fileExt = path.extname(file.name);
	var guid = Guid.raw()
	var fileName = guid + fileExt;
	var pathToFile = __dirname + "/images/";

	var image = new model.Image();
	image.title = req.body.title;
	image.filename = fileName;
	image.submitter = req.user.userId;
	file.mv(pathToFile + fileName).then(() => {
		return image.save()
	}).then(() => {
		return thumb({
			source: pathToFile + fileName,
			destination: pathToFile,
			width: 350
		});
	}).catch((err) => {
		if (err) {
			console.error(err);
			res.send(JSON.stringify(err));
		}
	}).then(() => {
		res.send(image);
	});





});


app.get('/api/images/', jwt({ secret: jwtSecret }), function (req, res) {

	model.Image
		.find()
		.sort({ "timestamp": -1 })
		.populate("submitter", "username")
		.then((images) => {
			res.send(images);
			res.end();
			return;
		})
});

app.get('/api/images/:id/', jwt({ secret: jwtSecret }), function (req, res) {
	model.Image
		.findOne({ _id: req.params.id })
		.populate("submitter", "username")
		.populate({
			path: "comments",
			options: {
				sort: {
					"timestamp": -1
				}
			},
			populate: {
				path: "submitter",
				select: "username"
			}
		})

		.exec(function (err, item) {
			res.send(item)
			res.end();
			return;
		})
});

app.post('/api/images/:id/comments', jwt({ secret: jwtSecret }), function (req, res) {
	console.log(req.params)
	var imageId = req.params.id;

	var comment = new model.Comment({
		content: req.body.comment,
		image: imageId,
		submitter: req.user.userId
	});
	comment.save()
		.catch(function () {
			res.status(500);
			res.end();
		})
		.then(function (createdComment) {
			return Promise.all([createdComment, model.Image.findOne({ _id: imageId })])
		}).then(function (array) {
			var createdComment = array[0];
			var relatedQuote = array[1];
			relatedQuote.comments.push(createdComment._id);
			return relatedQuote.save();
		}).catch(function () {
			res.status(500);
			res.end();
			next();
		}).then(function (t) {
			res.send();
			res.end();
			return;
		});

});


console.log("provisioned mongo url: " + process.env.MONGODB_URI)

app.use(function (err, req, res, next) {
	if (err);
	console.error(err);
	next();
})




app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	// intercept OPTIONS method
	if ('OPTIONS' == req.method) {
		console.log(req);
		res.send(200);
	}
	else {
		next();
	}
})




app.use(function (err, req, res, next) {

	if (err.name === 'UnauthorizedError') {
		console.log("handling unauthorized error");
		res.status(401).send({ error: "Access unauthorized" });
	} else {
		next();
	}
});

app.use((err, req, res, next) => {
	console.error(err.stack)
	next(err)
});


app.use(express.static(__dirname + "/webapp"));
app.use("/images", express.static(__dirname + "/images"));


app.listen(process.env.SERVER_PORT, function () {
	console.log('listening on port ' + process.env.SERVER_PORT + '!');
	console.log('http://localhost:' + process.env.SERVER_PORT);
});
