var app = angular.module('quoteApp', ['ngResource', 'ngRoute', 'ngCookies', 'infinite-scroll', 'angularFileUpload']);
angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250)
app.config(function ($routeProvider) {
	$routeProvider
		.when("/login", {
			controller: "LoginController",
			templateUrl: "templates/login/tmpl.login.html",
			requireLogin: false
		})
		.when("/signup", {
			controller: "LoginController",
			templateUrl: "templates/login/tmpl.login.html",
			requireLogin: false
		})
		.when("/quotes", {
			controller: "QuoteController",
			templateUrl: "templates/quotes/tmpl.quotes.html",
			requireLogin: true
		})
		.when("/quotes/:id/comments", {
			controller: "CommentQuoteController",
			templateUrl: "templates/quoteDetail/tmpl.quoteDetail.html",
			requireLogin: true
		})
		.when("/images", {
			controller: "ImageController",
			templateUrl: "templates/images/tmpl.images.html",
			requireLogin: true
		})
		.when("/images/:id/comments", {
			controller: "CommentImageController",
			templateUrl: "templates/imageDetail/tmpl.imageDetail.html",
			requireLogin: true
		})
		.when("/admin", {
			controller: "AdminController",
			templateUrl: "templates/admin/tmpl.admin.html",
			requireLogin: true,
			requireAdmin: true
		})
		.otherwise("/quotes")
})

app.config(['$httpProvider', function ($httpProvider, $http) {

	$httpProvider.interceptors.push(function ($q) {

		return {

			'responseError': function (rejection) {

				var defer = $q.defer();
				if (rejection.status == 401 || rejection.status == 403) {
					if (localStorage.token) {
						delete localStorage.token;
					}
				}

				defer.reject(rejection);

				return defer.promise;

			}
		};
	});

}]);

app.factory("User", function ($resource) {
	return $resource("/api/users/:id", {}, {
		query: {
			method: "GET",
			isArray: true
		},
		update: {
			method: "PUT"
		}
	})
})

app.factory("Quote", function ($resource) {
	return $resource("/api/quotes/:id", {}, {
		query: {
			method: "GET",
			isArray: true
		}
	})
})

app.factory("QuoteComment", function ($resource) {
	return $resource("/api/quotes/:id/comments", {}, {
		query: {
			method: "POST"
		}
	})
})

app.factory("ImageComment", function ($resource) {
	return $resource("/api/images/:id/comments", {}, {
		query: {
			method: "POST"
		}
	})
})

app.factory("Image", function ($resource) {
	return $resource("/api/images/:id", {}, {
		query: {
			method: "GET",
			isArray: true
		}
	})
})

app.factory("Auth", function ($http, $rootScope, $cookieStore, $resource, $location) {
	return {


		authorize: function () {
			return $rootScope.token;
		},

		loggedIn: function () {
			if (localStorage.token) {
				$rootScope.token = localStorage.token;
				$http.defaults.headers.common.Authorization = 'Bearer ' + localStorage.token;
			}

			return $rootScope.token !== undefined;
		},

		getToken: function () {
			return this.parseToken($rootScope.token);
		},

		parseToken: function (token) {
			if (!token) return;
			if (localStorage.parsedToken) return JSON.parse(localStorage.parsedToken);

			var base64Url = token.split('.')[1];
			var base64 = base64Url.replace('-', '+').replace('_', '/');
			localStorage.parsedToken = atob(base64)
			return JSON.parse(localStorage.parsedToken);
		},

		login: function (user) {
			var loginResult = $http.post("/login", user);
			return loginResult;
		},

		register: function (user) {
			return $http.post("/register", user);
		},

		logout: function () {
			if (localStorage.token)
				delete localStorage.token;
			if (localStorage.parsedToken)
				delete localStorage.parsedToken;
			if ($rootScope.token)
				delete $rootScope.token;

			$location.path("login");
		}

	}
})

app.directive('imageListItem', function () {
	return {
		templateUrl: 'templates/directives/image-list-item.html'
	};
});

app.run(function ($rootScope, $location, Auth) {
	var postLoginRoute;
	$rootScope.$on("$routeChangeStart", function (event, next, current) {
		if (!$rootScope.token && !Auth.loggedIn() || !localStorage.token) {
			$location.path("/login");
			return;
		}
	})
})