app.controller('AdminController', function ($scope, $rootScope, $routeParams, $location, Auth, User) {
    var loadUsers = function () { return User.query().$promise };
    loadUsers()
        .catch(function(exception){
            Auth.logout();
            $location.path("login");
        })
        .then(function (users) {
            $scope.users = users;
        })

    $scope.updateUsers = function () {
        User.update($scope.users);
    }
});