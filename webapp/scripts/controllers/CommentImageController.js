app.controller('CommentImageController', function ($scope, $rootScope, $routeParams, Image, ImageComment) {
	$scope.submitComment = function () {
		if($scope.newCommentText && $scope.newCommentText.length > 3){
			ImageComment
			.save({ id: $scope.image._id }, { comment: $scope.newCommentText }).$promise
			.then(function () {
				return loadComments()
			}).then(function(resources){
				$scope.image = resources;
				$scope.newCommentText = "";
			})
		}

	}


	var loadComments = function () { 
		return Image.get({ id: $routeParams.id }).$promise; 
	}


	loadComments().then(function (resources) {
		$scope.image = resources;
	})

})