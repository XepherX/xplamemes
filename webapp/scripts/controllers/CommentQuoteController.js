app.controller('CommentQuoteController', function ($scope, $rootScope, $routeParams, Quote, QuoteComment) {
	$scope.submitComment = function () {
		if($scope.newCommentText && $scope.newCommentText.length > 3){
			QuoteComment
			.save({ id: $scope.quote._id }, { comment: $scope.newCommentText }).$promise
			.then(function () {
				return loadComments()
			}).then(function(resources){
				$scope.quote = resources;
				$scope.newCommentText = "";
			})
		}

	}

	var loadComments = function () { 
		console.log("loading comments");
		return Quote.get({ id: $routeParams.id }).$promise; 
	}


	loadComments().then(function (resources) {
		$scope.quote = resources;
	})

})