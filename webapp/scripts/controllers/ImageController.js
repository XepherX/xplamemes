app.controller('ImageController', function ($scope, $rootScope, $routeParams, Image, FileUploader, Auth) {

    $scope.uploader = new FileUploader({
        url: "/api/images",
        headers: {
            "Authorization": "Bearer " + localStorage.token
        }
    });

    $scope.submitImage = () => {
        var queue = $scope.uploader.queue;
        for (var i = 0; i < queue.length; i++) {
            var file = queue[i];
            file.formData = [{
                "title": $scope.newImage.title
            }]
            file.onSuccess = (response, status, headers) => {
                $scope.Images.unshift(response);
                $scope.newImage = {}
            }
            file.upload()
        }


    }

    var loadImages = () => { return Image.query().$promise };

    loadImages().then(function (resources) {
        $scope.Images = resources;
    });

});