app.controller('LoginController', function ($scope, $rootScope, $routeParams, $location, Auth, $log) {
	$scope.signup = function (ctx) {
		Auth.register($scope.signupUser).then(function (res) {
			return $scope.performLogin($scope.signupUser.username, $scope.signupUser.password);
		});
	}
	$scope.isActive = function (viewLocation) {
		return viewLocation === $location.path();
	};
	$scope.login = function () {
		$scope.performLogin($scope.email, $scope.password)
	}
	$scope.performLogin = function (email, password) {
		Auth.login({ email: email, password: password })
			.then(function (res) {
				if (res.data.token) {
					$rootScope.token = res.data.token
					localStorage.token = res.data.token
					$location.path("quotes");
				}
			})
			.catch(function (error) {
				if (error.status === 403) {
					$scope.loginForm.$error.invalidPassword = true;
				}
			});
	}

})