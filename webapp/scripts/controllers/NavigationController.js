app.controller('NavigationController', function ($scope, $rootScope, $routeParams, $location, Auth) {
    $scope.isAdmin = function () {
        if (!$rootScope.token) return false;
        var tokenContent = Auth.getToken();
        return tokenContent.isAdmin===true;
    }

    $scope.doLogout = () => {
        Auth.logout();
    }

    $scope.isLoggedIn = () => {
        return Auth.loggedIn();
    }

    $scope.loggedOut = function () { return !Auth.loggedIn() }();

    $scope.isLoginPath = function () { return $location.$$path == '/login' }
});