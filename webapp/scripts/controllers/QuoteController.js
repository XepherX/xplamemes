app.controller('QuoteController', function ($scope, $rootScope, $routeParams, Quote) {
	$scope.loading = true;
	$scope.routeParams = $routeParams;

	$scope.submitQuote = function () {
		Quote.save($scope.newQuote).$promise
			.then(function () {
				return loadQuotes()
			}).then(function (resources) { 
				$scope.quotes = resources;
			})
	}

	$scope.scrollPagination = function () {
		if (!$scope.quotes) return;
		var paginationId = $scope.quotes[$scope.quotes.length - 1]._id;
		loadQuotes(paginationId).then((loadedQuotes) => {
			if(loadedQuotes.length == 0) $scope.reachedEnd = true;
			$scope.quotes = $scope.quotes.concat(loadedQuotes);
		});

	}

	var loadQuotes = function (paginationId) {
		let query;
		if (paginationId){
			query = Quote.query({ pagination: paginationId });
		}
		else{
			query = Quote.query();
		}
		return query.$promise
	};

	loadQuotes().then(function (resources) {
		if(resources.length == 0) $scope.reachedEnd = true;
		$scope.quotes = resources;
		$scope.loading = false;
	});


});